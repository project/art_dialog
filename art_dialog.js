

if (Drupal.jsEnabled) {
	Drupal.behaviors.art_dialog = function() {
   var msg = $('#artdialog_message_wrapper').html();
   if (msg) {
     $.dialog({
       id:   'art_msg',  //id
       skin: 'art-msg', //skin is used to class
       content: msg,
       width: parseInt(Drupal.settings.artDialog.width),
       title: Drupal.settings.artDialog.title
     });
   }
 }
}