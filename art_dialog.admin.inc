<?php 
// $Id: better_messages.inc,v 1.1.2.5 2010/03/29 09:56:01 doublethink Exp $

/*
 * Admin settings menu callback
 */
function art_dialog_settings_form() {
	 $settings = art_dialog_get_settings();
  $path = drupal_get_path('module', 'art_dialog');
  
  $form['version'] = array(
    '#type'          => 'select',
    '#title'         => t('Select artDialog version'),
    '#default_value' => $settings['version'],
    '#options'       => _art_dialog_list_files($path . '/artDialog*'), 
    '#description'   => t('Put artDialog js package into art_dialog module directory and name it to artDialog*')
            . '<br/>' . t('Note that, if using var_dump in artDialog5, it will cause js error and artDialog won\'t work due to document.compatMode is BackCompat')
  );

  $form['override'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Override drupal message'),
    '#default_value' => $settings['override'],
  );

  $form['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('The title of status message box'),
    '#default_value' => $settings['title'],
    '#size'          => 20,
    '#maxlength'     => 20,
 	);
  
  $form['width'] = array(
    '#type'          => 'textfield',
    '#title'         => t('The width of message box for Drupal message'),
    '#default_value' => $settings['width'],
    '#size'          => 20,
    '#maxlength'     => 20,
 	);
  
  
  $form['visibility'] = array(
    '#type'          => 'radios',
    '#title'         => t('Exclude/Include for specified pages'),
    '#options'       => array(0 => 'exlcude', 1 => 'include'),
    '#default_value' => $settings['visibility'],
  );
  
  $form['visible_pages'] = array(
    '#type'  => 'textarea',
    '#title' => t('Include/Exclude pages'),
    '#default_value' => $settings['visible_pages'],
    '#description'   => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );
  
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['#theme']    = 'system_settings_form';
  $form['#submit'][] = 'art_dialog_form_submit';
  return $form;
}

function art_dialog_form_submit($form, &$form_state) {
  $settings = art_dialog_get_settings();
  $submitted   = array('version', 'override', 'title', 'width', 
                       'visibility', 'visible_pages', 
                      'skin', 'plugin');
  
  foreach ($form_state['values'] as $key => $val) {
    if (in_array($key, $submitted)) {
      $settings[$key] = $val;
    }
  }
  
  variable_set('art_dialog_settings', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
  cache_clear_all();
}

function art_dialog_skins_setting() {
  $path  = drupal_get_path('module', 'art_dialog');
  $url   = url($path . '/skins-demo.html');
  return drupal_get_form('art_dialog_skins_form') 
   . "<h3>This example is using artDialog5.</h3>" 
   . "<iframe src='$url' width='660' height='200' scrolling='no' frameborder='no'>"
   . "</iframe>";
}

function art_dialog_skins_form() {
  $settings = art_dialog_get_settings();
  $path     = drupal_get_path('module', 'art_dialog');
  $pattern  = "$path/{$settings['version']}/skins/*.css";
  $form['skin'] = array(
    '#type'          => 'select',
    '#title'         => t('Select artDialog version'),
    '#default_value' => $settings['skin'],
    '#options'       => _art_dialog_list_files($pattern), 
    '#description'   => t('You are using !version, please select the skin for current version of artDialog', array('!version' => $settings['version']))
  );
  
  if ($settings['version'] == 'artDialog5') {
    $form['plugin'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Enable !version plugin', array('!version' =>$settings['version'])),
      '#default_value' => $settings['plugin'],
    );
  }
  
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['#theme']    = 'system_settings_form';
  $form['#submit'][] = 'art_dialog_form_submit';
  return $form;
  
}

/**
 * Inner help function
 * @param type $pattern
 * @return type 
 */
function _art_dialog_list_files($pattern) {
  $array  = glob($pattern);
  $return = array();
  foreach ($array as $k => $v) {
    $file   = basename($v);
    $return[$file] = $file;
  }
  return $return;
}